import Price from './Price.js';

export default class Symbol {
  constructor(symbol, tsyms = 'USD') {
    this.symbol = symbol;
    this.tsyms = tsyms;
    this.price = new Price(this.symbol, this.formatTsyms());
  }

  formatTsyms() {
    return this.tsyms.join(',');
  }

  async getPrice() {
    // eslint-disable-next-line no-return-await
    return await this.price.get();
  }
}
