import express from 'express';
import Storage from '../Storage.js';

const router = express.Router();

router.get('/user/:id', async (req, res, next) => {
  const storage = new Storage();
  const { id } = req.params;
  let user = storage.getByUserId(id);
  if (!user) {
    res.status(404).end();
  } else {
    const BTC = storage.getBySymbol('BTC');
    user = Object.assign(user, BTC);
    res.json({ data: user });
  }
});

export default router;
