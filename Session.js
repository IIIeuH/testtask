import { v4 } from 'uuid';

export default class Session {
  constructor(exp = 24 * 60 * 60 * 1000) {
    this.exp = exp;
  }

  new(socketId) {
    return {
      userId: socketId,
      sessionId: v4(),
      exp: new Date().getTime() + this.exp,
    };
  }
}
