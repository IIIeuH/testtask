import express from 'express';
import router from './routes/route.js';
import config from './config.js';
import Symbol from './Symbol.js';
import Storage from './Storage.js';
import './Socket.js';
import './Client.js';

const app = express();

app.use('/', router);

const currentValue = async () => {
  const symbol = new Symbol('BTC', ['USD']);
  const usd = await symbol.getPrice();
  const storage = new Storage();
  storage.addSymbol('BTC', usd);
};

setInterval(async () => {
  await currentValue();
}, 1000 * 60);

app.listen(config.port, async () => {
  await currentValue();
  // eslint-disable-next-line no-console
  console.info(`Server run on port ${config.port}`);
});
