import { createServer } from 'http';
import { Server } from 'socket.io';

import config from './config.js';
import Session from './Session.js';
import Storage from './Storage.js';

const httpServer = createServer();
const io = new Server(httpServer);
const storage = new Storage();
httpServer.listen(config.sPort);

io.on('connection', (socket) => {
  // eslint-disable-next-line no-console
  console.info(`New connections. id: ${socket.id}`);
  const session = new Session();
  const userSession = session.new(socket.id);
  storage.addUser(userSession);
  socket.on('disconnect', () => {
    storage.remove(socket.id);
  });
});
