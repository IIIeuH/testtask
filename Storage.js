export default class Storage {
  constructor() {
    if (Storage.exist) {
      return Storage.instance;
    }
    Storage.instance = this;
    Storage.exist = true;
    this.users = [];
    this.symbolsPrice = new Map();
  }

  getByUserId(userId) {
    return this.users.find((s) => s.userId === userId);
  }

  getBySymbol(symbol) {
    return this.symbolsPrice.get(symbol);
  }

  addUser(data) {
    this.users.push(data);
  }

  addSymbol(symbol, data) {
    this.symbolsPrice.set(symbol, data);
  }

  remove(userId) {
    const index = this.users.findIndex((s) => s.userId === userId);
    this.users.splice(index, 1);
  }
}
