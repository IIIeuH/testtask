module.exports = {
  env: {
    es2021: true,
  },
  plugins: ['prettier'],
  extends: ['airbnb', 'prettier'],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'prettier/prettier': ['error', { singleQuote: true }],
    'import/extensions': 'off',
    semi: ['error', 'always'],
  },
};
