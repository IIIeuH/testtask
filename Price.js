import axios from 'axios';

export default class Price {
  constructor(fsym, tsyms) {
    this.fsym = fsym;
    this.tsyms = tsyms;
  }

  async get() {
    const res = await axios.get(
      `https://min-api.cryptocompare.com/data/price?fsym=${this.fsym}&tsyms=${this.tsyms}`
    );
    return res.data;
  }
}
